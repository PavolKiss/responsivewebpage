const imgContainer = document.getElementById("imgContainer");
const next = document.getElementById("next");
const prev = document.getElementById("prev");
const boxes = document.querySelectorAll(".shadowBorder, .sizeShadow");
const boxesArray = Array.from(boxes);

const images = [
  "hoodie1.png",
  "hoodie2.png",
  "hoodie3.png",
  "hoodie4.png",
  "hoodie5.png",
  "hoodie6.png"
];
const text = [
  "pro gamer slash hoodie",
  "spectrum gamer tee",
  "for gamers by gamers tee",
  "pro gamer slash hoodie",
  "spectrum gamer tee",
  "for gamers by gamers tee"
];

let i = images.length && text.length;

next.onclick = () => {
  i = i < images.length ? (i = i + 1) : (i = 1);
  imgContainer.innerHTML =
    "<img class='hoodies img' src=image/" +
    images[i - 1] +
    "> <div class='imageTitles'>" +
    text[i - 1] +
    "</div>";
};

prev.onclick = () => {
  i = i < images.length + 1 && i > 1 ? (i = i - 1) : (i = images.length);
  imgContainer.innerHTML =
    "<img class='hoodies img' src=image/" +
    images[i - 1] +
    "> <div class='imageTitles'>" +
    text[i - 1] +
    "</div>";
};

function show(id) {
  document.getElementById(id).style.display = "flex";
}

function hide(id) {
  document.getElementById(id).style.display = "none";
}

function toggleShadow() {
  let i = 0;

  for (i; i < boxesArray.length; i++) {
    boxesArray[i].classList.remove("addShadow");
  }
  this.classList.add("addShadow");
}

boxesArray.forEach(box => {
  box.addEventListener("click", toggleShadow);
});
